#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "hash.h"



// from https://github.com/jhallen/joe-editor/blob/master/joe/hash.c
#define _hnext(accu, c) (((accu) << 4) + ((accu) >> 28) + (c))
static unsigned long _hashFunc(const char *key)
{
  const char *keyp = key;
  unsigned long accu = 0;
  while (*keyp) {
    accu = _hnext(accu, *keyp++);
  }
  return accu;
}

struct Hash* hnew(int size){
  struct Hash *h = calloc(1, sizeof(struct Hash));
  h->size = size;
  h->table = calloc(size, sizeof(struct HashNode*));
  //(struct HashNode**) malloc(sizeof(struct HashNode*) * size);
  //memset((void*)h->table, 0, sizeof(struct HashNode*) * size);
  return h;
}

void hdelete(struct Hash **hash){
  if (hash == NULL || *hash == NULL) return;
  struct Hash *h = *hash;
  for (int i = 0; i < h->size; i++) {
    if (h->table[i] != NULL){
      struct HashNode *n = h->table[i], *next;
      while (n){
        next = n->next;
        free(n->key);
        free(n);
        n = next;
      }
    }
  }
  free(*hash);
  *hash = NULL;
}

static struct HashNode* _hashSet(struct Hash *hash, const char *key){
  int idx = _hashFunc(key) % hash->size;
  struct HashNode *node = malloc(sizeof(struct HashNode));
  node->key = strdup(key);//(char*) malloc(sizeof(char) * (strlen(key) + 1) ); strcpy(node->key, key);
  node->next = NULL;
  //colision! search last node
  if (hash->table[idx]){
    struct HashNode *ni = hash->table[idx];
    while (ni->next) {
      ni = ni->next;
    }
    ni->next = node;
  }
  else{
    hash->table[idx] = node;
  }

  hash->length++;
  return node;
}

void hsetc(struct Hash *hash, const char *key, char value){
  struct HashNode *node = _hashSet(hash, key);
  node->value.c = value;
  node->value_type = HASH_CHAR;
  node->value_size = sizeof(char);
}
void hseti(struct Hash *hash, const char *key, int value){
  struct HashNode *node = _hashSet(hash, key);
  node->value.i = value;
  node->value_type = HASH_INT;
  node->value_size = sizeof(int);
}
void hsetui(struct Hash *hash, const char *key, unsigned int value){
  struct HashNode *node = _hashSet(hash, key);
  node->value.ui = value;
  node->value_type = HASH_UINT;
  node->value_size = sizeof(unsigned int);
}
void hsetf(struct Hash *hash, const char *key, float value){
  struct HashNode *node = _hashSet(hash, key);
  node->value.f = value;
  node->value_type = HASH_FLOAT;
  node->value_size = sizeof(float);
}
void hsets(struct Hash *hash, const char *key, char *value){
  struct HashNode *node = _hashSet(hash, key);
  node->value.s = value;
  node->value_type = HASH_STRING;
  node->value_size = sizeof(char) * strlen(value);
}
void hsetp(struct Hash *hash, const char *key, void *value){
  struct HashNode *node = _hashSet(hash, key);
  node->value.p = value;
  node->value_type = HASH_POINTER;
  node->value_size = sizeof(void*);
}

void hunset(struct Hash *hash, const char *key){
  int idx = _hashFunc(key) % hash->size;
  struct HashNode *n = hash->table[idx], *p = NULL;
  for(; n && strcmp(n->key, key) != 0; p = n, n = n->next);
  if (n){
    if (!p) { hash->table[idx] = n->next; }
    else { p->next = n->next; };
    free(n->key);
    free(n);
    hash->length--;
  }    
}

struct HashNode* hget(struct Hash *hash, const char *key){
  int idx = _hashFunc(key) % hash->size;
  struct HashNode *n = hash->table[idx];
  for(; n && strcmp(n->key, key) != 0; n = n->next);
  return n;
}

char hgetc(struct Hash *hash, const char *key, char default_value){
  struct HashNode *n = hget(hash, key);
  return n == NULL ? default_value: n->value.c;
}
int hgeti(struct Hash *hash, const char *key, int default_value){
  struct HashNode *n = hget(hash, key);
  return n == NULL ? default_value: n->value.i;
}
unsigned int hgetui(struct Hash *hash, const char *key, unsigned int default_value){
  struct HashNode *n = hget(hash, key);
  return n == NULL ? default_value: n->value.ui;
}
float hgetf(struct Hash *hash, const char *key, float default_value){
  struct HashNode *n = hget(hash, key);
  return n == NULL ? default_value: n->value.f;
}
char* hgets(struct Hash *hash, const char *key, char *default_value){
  struct HashNode *n = hget(hash, key);
  return n == NULL ? default_value: n->value.s;
}
void* hgetp(struct Hash *hash, const char *key, void *default_value){
  struct HashNode *n = hget(hash, key);
  return n == NULL ? default_value: n->value.p;
}

void hashTraverse(struct Hash *hash, void (*traverseFunc)(void *p)){
  if (traverseFunc == NULL) return;
  struct HashNode *n;
  for (int i = 0; i < hash->size; i++)
  {
    if (hash->table[i]){
      int count = 0;
      n = hash->table[i];
      while(n){
        switch(n->value_type){
          case HASH_CHAR:    traverseFunc(&(n->value.c)); break;
          case HASH_INT:     traverseFunc(&(n->value.i)); break;
          case HASH_UINT:    traverseFunc(&(n->value.ui)); break;
          case HASH_FLOAT:   traverseFunc(&(n->value.f)); break;
          case HASH_STRING:  traverseFunc(n->value.s);    break;
          case HASH_POINTER: traverseFunc(n->value.p);    break;
        }
        n = n->next;
        count++;
      }
    }
  }
}

void hashDebug(struct Hash *hash, int full_debug, void (*debugFunc)(void *p)){
  char *TYPES[] = {"char", "int", "uint", "float", "string", "pointer"};
  struct HashNode *n;
  for (int i = 0; i < hash->size; i++)
  {
    if (hash->table[i]){
      int count = 0;
      printf("%i:", i);
      n = hash->table[i];
      while(n){
        if (full_debug){
          printf("\t\"%s\": (%s) ", n->key, TYPES[n->value_type]);
          switch(n->value_type){
            case HASH_CHAR: printf("\'%c\'\n", n->value.c); break;
            case HASH_INT : printf("%i\n", n->value.i); break;
            case HASH_UINT : printf("%u\n", n->value.ui); break;
            case HASH_FLOAT: printf("%f\n", n->value.f); break;
            case HASH_STRING: printf("\"%s\"\n", n->value.s); break;
            case HASH_POINTER: 
              printf("%p\n", n->value.p); 
              if (debugFunc != NULL) debugFunc(n->value.p);
              break;
          }
        }
        n = n->next;
        count++;
      }
      printf(" %i items\n", count);
    }
    else printf("%i: 0 items\n", i);
  }
  printf("%i items in hash.\n", hash->length);
}