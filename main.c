#include <math.h>
#include "graphics.h"
#include "game_scene.h"

Texture *tex_tileup, *tex_tileupbomb, *tex_tiledown, *tex_tile1, *tex_tile2, *tex_tile3, *tex_tile4, *tex_tile5, *tex_tile6, *tex_tile7, *tex_tile8, *tex_tileflag, *tex_tilebomb, *tex_tileko; 
Texture *tex_numbers[11], *tex_smiley;

int main(int argc, char *argv[]){
  double time, totaltime;
  unsigned frames = 0;
  GLFWwindow* window = openWindow(WIDTH, HEIGHT, "BuscaPipas", false, false);

  loadAtlas("assets.atlas");
  tex_tileup = getTexture("tileup.png");
  tex_tiledown = getTexture("tiledown.png");
	tex_tile1 = getTexture("tile1.png");
	tex_tile2 = getTexture("tile2.png");
	tex_tile3 = getTexture("tile3.png");
	tex_tile4 = getTexture("tile4.png");
	tex_tile5 = getTexture("tile5.png");
	tex_tile6 = getTexture("tile6.png");
	tex_tile7 = getTexture("tile7.png");
	tex_tile8 = getTexture("tile8.png");
	tex_tileflag = getTexture("tileflag.png");
	tex_tilebomb = getTexture("tilebomb.png");
	tex_tileupbomb = getTexture("tileupbomb.png");
	tex_tileko = getTexture("tileko.png");

	for (int i = 0; i < 10; i++) {
		char name[10];
		sprintf(name, "%i.png", i);
		tex_numbers[i] = getTexture(name);
	}
	tex_numbers[10] = getTexture("dots.png");
	tex_smiley = getTexture("smile.png");

	//GLuint uniform_time = glGetUniformLocation(program, "time");

  GLuint framebuffer = createFramebuffer();

  while(!glfwWindowShouldClose(window)){
    time = glfwGetTime();
	
		updateEvents(window);	

		gamescene_processEvents(window);

		//glUniform1f(uniform_time, time);

		beginFramebuffer(framebuffer);

    gamescene_render(time);

		render();

		endFramebuffer(framebuffer);
		glfwSwapBuffers(window);
    totaltime += glfwGetTime() - time;
    frames++;
  }

  deleteTextures();
	closeWindow();
  printf("ms: %.3f | FPS: %.1f\n", totaltime / frames, (float)frames / time);
	return 0;
}
