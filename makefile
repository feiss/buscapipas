OBJ     = obj/main.o obj/graphics.o obj/hash.o obj/vendor/lodepng.o obj/game_scene.o
HEADERS = main.c graphics.h hash.h game_scene.h vendor/lodepng.h
CC = gcc

DEBUG = -Wall -g -D DEBUG
#DEBUG = -D RELEASE -Os -finline-small-functions

ifeq ($(OS),Windows_NT)
	LIBS = -lglfw3dll -lopengl32 -lglew32 -D WIN 
else 
  UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		LIBS = -lGL -lglfw -lGLEW -lm -D LINUX 
	endif
	ifeq ($(UNAME_S),Darwin)
		LIBS += -D MAC 
	endif
endif

all: buscapipas

obj/vendor:
	mkdir -p obj/vendor

obj/%.o: %.c
	$(CC) -c $< -o $@ $(DEBUG)

obj/vendor/%.o: vendor/%.c 
	$(CC) -c $< -o $@ $(DEBUG)

buscapipas: obj/vendor $(OBJ) $(HEADERS)
	$(CC) -o$@ $(OBJ) $(LIBS) $(DEBUG)



.PHONY: clean
clean:
	rm -f app debug
	rm -rf obj
