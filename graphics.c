#include <unistd.h>
#include "graphics.h"
#include "hash.h"

static struct Hash *textures = NULL;
static int MAX_TEXTURE_SIZE = 4096;

// x, y, u, v, r, g, b, a, d1, d2
//   0--1  /3
//   | /  / |
//   2/  5--4
typedef struct{
  float x, y;
  float u, v;
  unsigned char r, g, b, a;
  float d1, d2;
} SpriteVertex;

static const float VERTEX_OFFSET_X[] = {0.f, 1.f, 0.f, 1.f, 1.f, 0.f};
static const float VERTEX_OFFSET_Y[] = {0.f, 0.f, 1.f, 0.f, 1.f, 1.f};
static const float UV_OFFSET_X[] = {0.f, 1.f, 0.f, 1.f, 1.f, 0.f};
static const float UV_OFFSET_Y[] = {0.f, 0.f, 1.f, 0.f, 1.f, 1.f};

static SpriteVertex vertex_data[6 * MAX_SPRITES];
static struct{
	int start;
	Texture *texture;
} texture_batches[MAX_SPRITES];

static int num_sprites = 0;
static int num_texture_batches = 0;

static int num_live_vars = 0;
static int curr_live_var = 0;
struct {
  int *pointer;
  int delta;
  char name[20];
} live_vars[100];

// File utils

void filename(const char *path, char *fname){
  char *p = strrchr(path, '/');
  if (p == NULL) strcpy(fname, path);
  else strcpy(fname, p+1);
}

int filesize(const char *path){
  FILE *f = fopen(path, "rt");
  if (!f) {
    ERROR("file %s not found\n", path);
    return 0;
  }
  fseek(f, 0, SEEK_END);
  int size = ftell(f);
  fclose(f);
  return size;
}

int fileexists(const char *path){
  if( access( path, F_OK ) != -1 ) { return 1; } else { return 0; }
}

char *readfile(const char *path){
  printf("Loading %s\n", path);
  FILE *f = fopen(path, "rt");
  if (!f) {
    ERROR("file %s not found\n", path);
    return NULL;
  }
  fseek(f, 0, SEEK_END);
  int size = ftell(f);
  fseek(f, 0, SEEK_SET);
  char *buf = malloc(size + 1);
  buf[size] = 0;
  fread((void*)buf, 1, size, f);
//  printf("%s\n%i bytes read\n\n", buf, read);
  fclose(f);
  return buf;
}

void ERROR(const char* msg, ...)
{
  va_list argptr;
  va_start(argptr, msg);
#ifdef LINUX
  vfprintf(stderr, "\x1b[31mERROR: \x1b[0m%s", argptr);
#else
  vfprintf(stderr, "ERROR: %s", argptr);
#endif
  va_end(argptr);
  glfwTerminate();
  exit(-1);
}


// Window initialization and handling

void APIENTRY glDebugOutput(
  unsigned int source, 
  unsigned int type, 
  unsigned int id, 
  unsigned int severity, 
  int length, 
  const char *message, 
  const void *userParam) {
  // ignore non-significant error/warning codes
  if(id == 131169 || id == 131185 || id == 131218 || id == 131204) return; 

//    if (severity != GL_DEBUG_SEVERITY_HIGH) return;

  printf("------\nOpenGL | %s | ", message);

  switch (source)
  {
      case GL_DEBUG_SOURCE_API:             printf("Source: API |"); break;
      case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   printf("Source: Window System |"); break;
      case GL_DEBUG_SOURCE_SHADER_COMPILER: printf("Source: Shader Compiler |"); break;
      case GL_DEBUG_SOURCE_THIRD_PARTY:     printf("Source: Third Party |"); break;
      case GL_DEBUG_SOURCE_APPLICATION:     printf("Source: Application |"); break;
      case GL_DEBUG_SOURCE_OTHER:           printf("Source: Other |"); break;
  }
  switch (type)
  {
      case GL_DEBUG_TYPE_ERROR:               printf("Type: Error |"); break;
      case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: printf("Type: Deprecated Behaviour |"); break;
      case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  printf("Type: Undefined Behaviour |"); break; 
      case GL_DEBUG_TYPE_PORTABILITY:         printf("Type: Portability |"); break;
      case GL_DEBUG_TYPE_PERFORMANCE:         printf("Type: Performance |"); break;
      case GL_DEBUG_TYPE_MARKER:              printf("Type: Marker |"); break;
      case GL_DEBUG_TYPE_PUSH_GROUP:          printf("Type: Push Group |"); break;
      case GL_DEBUG_TYPE_POP_GROUP:           printf("Type: Pop Group |"); break;
      case GL_DEBUG_TYPE_OTHER:               printf("Type: Other |"); break;
  }
  switch (severity)
  {
      case GL_DEBUG_SEVERITY_HIGH:         printf("Severity: high"); break;
      case GL_DEBUG_SEVERITY_MEDIUM:       printf("Severity: medium"); break;
      case GL_DEBUG_SEVERITY_LOW:          printf("Severity: low"); break;
      case GL_DEBUG_SEVERITY_NOTIFICATION: printf("Severity: notification"); break;
  }
  printf("\n");
}

bool checkShaderCompilation(GLuint shader){
  GLint status;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  if (status == GL_FALSE){
    GLint blen = 0; 
    GLsizei slen = 0;

    glGetShaderiv(shader, GL_INFO_LOG_LENGTH , &blen);       
    if (blen > 1)
    {
      GLchar* compiler_log = malloc(blen);
      glGetInfoLogARB(shader, blen, &slen, compiler_log);
      ERROR("SHADER: %s\n", compiler_log);
      free (compiler_log);
    }
  }
  return status;
}

void initShaders(){
	const char* vertex_shader =
		"#version 400\n"
		"layout(location = 0) in vec2 inPos;"
		"layout(location = 1) in vec2 inUV;"
		"layout(location = 2) in vec4 inCol;"
		"layout(location = 3) in vec2 inData;"
		"out vec4 color;"
		"out vec2 uv;"
		"out vec2 data;"
		"void main() {"
		"  color = inCol;"
		"  uv = inUV;"
		"  data = inData;"
		"  gl_Position = vec4(inPos, 0.0, 1.0);"
		"}";

	const char* fragment_shader =
		"#version 400\n"
		"in vec4 color;"
		"in vec2 uv;"
		"in vec2 data;"
		"out vec4 outColor;"
		"uniform sampler2D texture;"
		//"uniform float time;"
		"void main() {"
		"  outColor = color * texture2D(texture, uv);"
		"}";

	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vertex_shader, NULL);
	glCompileShader(vs);
  if (!checkShaderCompilation(vs)){
  	ERROR("Vertex shader compilation error.");
  }

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragment_shader, NULL);
	glCompileShader(fs);
  if (!checkShaderCompilation(fs)){
  	ERROR("Fragment shader compilation error.");
  }

	GLuint program = glCreateProgram();
	glAttachShader(program, fs);
	glAttachShader(program, vs);
	glLinkProgram(program);
  glUseProgram(program);
}


void initBuffers(){
	GLuint vbo;
	glGenBuffers(1, &vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), NULL, GL_DYNAMIC_DRAW);

	unsigned int vao;
	glGenVertexArrays(1, &vao); 
	glBindVertexArray(vao);

	// x, y, u, v, r, g, b, a, d1, d2
	// xy
  glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), (void*)0);
  // uv
  glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), (void*)(2 * sizeof(float)));
	// rgba
  glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(SpriteVertex), (void*)(4 * sizeof(float)));
	// d1 d2
  glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), (void*)(4 * sizeof(float) + 4 * sizeof(unsigned char)));
}


static int _keystate[400];
static double _mousex = 0.0, _mousey = 0.0;
static bool _mouse_button_left_prev = false, _mouse_button_right_prev = false, _mouse_button_middle_prev = false;
static bool _mouse_button_left = false, _mouse_button_right = false, _mouse_button_middle = false;

void updateEvents(GLFWwindow *window){
	glfwPollEvents();

  glfwGetCursorPos(window, &_mousex, &_mousey);
  _mouse_button_left_prev   = _mouse_button_left;
  _mouse_button_right_prev  = _mouse_button_right;
  _mouse_button_middle_prev = _mouse_button_middle;
  _mouse_button_left   = glfwGetMouseButton(window, 0) == GLFW_PRESS;
  _mouse_button_right  = glfwGetMouseButton(window, 1) == GLFW_PRESS;
  _mouse_button_middle = glfwGetMouseButton(window, 2) == GLFW_PRESS;

	int i, key, action;
	for (i = 0; i < 400; i++){
		key = i;
		action = glfwGetKey(window, key);

		if (action == GLFW_PRESS) {
			switch(_keystate[key]){
				case KEY_JUST_RELEASED: 
				case KEY_NOT_PRESSED:   _keystate[key] = KEY_JUST_PRESSED; break;
				case KEY_JUST_PRESSED:  _keystate[key] = KEY_HELD; break;
			}
		}
		else if (action == GLFW_RELEASE) {
			switch(_keystate[key]){
				case KEY_HELD:
				case KEY_JUST_PRESSED:  _keystate[key] = KEY_JUST_RELEASED; break;
				case KEY_JUST_RELEASED: _keystate[key] = KEY_NOT_PRESSED; break;
			}
		}
		if (key == GLFW_KEY_LEFT_SHIFT || key == GLFW_KEY_RIGHT_SHIFT) {
			_keystate[GLFW_KEY_SHIFT] = _keystate[key];
		}
		else if (key == GLFW_KEY_LEFT_ALT || key == GLFW_KEY_RIGHT_ALT) {
			_keystate[GLFW_KEY_ALT] = _keystate[key];
		}
		else if (key == GLFW_KEY_LEFT_CONTROL || key == GLFW_KEY_RIGHT_CONTROL) {
			_keystate[GLFW_KEY_CONTROL] = _keystate[key];
		}
	}

#ifdef EXIT_ON_ESCAPE
	if (keyJustReleased(GLFW_KEY_ESCAPE)){
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
#endif

  if (num_live_vars > 0 && keyPressed(GLFW_KEY_LEFT_SHIFT)){
    if (keyPressed(GLFW_KEY_LEFT_CONTROL)){
      if (keyJustReleased(GLFW_KEY_RIGHT)){
        curr_live_var = (curr_live_var + 1) % num_live_vars;
        printf("Live var %i: %s (%i)\n", curr_live_var, live_vars[curr_live_var].name, *(live_vars[curr_live_var].pointer));
      }
      if (keyJustReleased(GLFW_KEY_LEFT)){
        curr_live_var = (curr_live_var - 1) % num_live_vars;
        printf("Live var %i: %s (%i)\n", curr_live_var, live_vars[curr_live_var].name, *(live_vars[curr_live_var].pointer));
      }
    }
    else {
      if (keyJustReleased(GLFW_KEY_RIGHT)){
        *(live_vars[curr_live_var].pointer) += live_vars[curr_live_var].delta;
        printf("%s => %i\n", live_vars[curr_live_var].name, *(live_vars[curr_live_var].pointer));
      }
      if (keyJustReleased(GLFW_KEY_LEFT)){
        *(live_vars[curr_live_var].pointer) -= live_vars[curr_live_var].delta;
        printf("%s => %i\n", live_vars[curr_live_var].name, *(live_vars[curr_live_var].pointer));
      }
    }
  }

}

int    getKeyState(int key) { return _keystate[key]; }
bool   keyNotPressed(int key){ return _keystate[key] == KEY_NOT_PRESSED; }
bool   keyJustPressed(int key){ return _keystate[key] == KEY_JUST_PRESSED; }
bool   keyPressed(int key){ return _keystate[key] == KEY_HELD; }
bool   keyJustReleased(int key){ return _keystate[key] == KEY_JUST_RELEASED; }
double getMouseX() { return _mousex; }
double getMouseY() { return _mousey; }
bool   leftJustPressed()   { return !_mouse_button_left_prev && _mouse_button_left; }
bool   rightJustPressed()  { return !_mouse_button_right_prev && _mouse_button_right; }
bool   middleJustPressed() { return !_mouse_button_middle_prev && _mouse_button_middle; }
bool   leftJustReleased()   { return _mouse_button_left_prev && !_mouse_button_left; }
bool   rightJustReleased()  { return _mouse_button_right_prev && !_mouse_button_right; }
bool   middleJustReleased() { return _mouse_button_middle_prev && !_mouse_button_middle; }
bool   leftPressed()   { return _mouse_button_left; }
bool   rightPressed()  { return _mouse_button_right; }
bool   middlePressed() { return _mouse_button_middle; }

GLFWwindow* openWindow(int w, int h, const char *title, bool fullscreen, bool debug){

  setlocale (LC_ALL, "");
  glfwInit();

//  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

  if (debug) glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);  
  //glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  GLFWwindow *window;
  if (fullscreen){
    window = glfwCreateWindow(w, h, title, glfwGetPrimaryMonitor(), NULL);
  }
  else {
    window = glfwCreateWindow(w, h, title, NULL, NULL);
  }

  const GLFWvidmode* video_mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
  glfwSetWindowPos(window, video_mode->width / 2 - w / 2, video_mode->height / 2 - h / 2);
//  glfwSetCursorPos(window, w / 2, h / 2);

  if (!window) { ERROR("could not create window\n"); }
  
  glfwMakeContextCurrent(window);
  
  glewExperimental = GL_TRUE;

  if (glewInit() != GLEW_OK) { ERROR("Failed to initialize GLEW"); }

  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_FALSE);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glGetIntegerv(GL_MAX_TEXTURE_SIZE, &MAX_TEXTURE_SIZE);

  if (debug){
    GLint flags; 
    glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
    if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
      glEnable(GL_DEBUG_OUTPUT);
      glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS); 
      glDebugMessageCallback(glDebugOutput, NULL);
      //GL_DONT_CARE
      glDebugMessageControl(GL_DEBUG_SOURCE_API, GL_DEBUG_TYPE_ERROR, GL_DEBUG_SEVERITY_HIGH, 0, NULL, GL_TRUE);
      printf("OpenGL debug initialized\n");   
    }
  }

  memset(_keystate, 0, 400 * sizeof(int));

  initBuffers();
  initShaders();

  return window;
}

void closeWindow(){

  glfwTerminate();

  if (num_live_vars > 0){
    printf("\n");
    for (int i = 0; i < num_live_vars; i++){
      printf("  %s: %i\n", live_vars[i].name, *(live_vars[i].pointer));
    }
    printf("\n");
  }
}


void setLiveVar(char *name, int *var, int delta){
  if (num_live_vars >= 100) { return; }
  live_vars[num_live_vars].pointer = var; 
  live_vars[num_live_vars].delta = delta; 
  strcpy(live_vars[num_live_vars].name, name); 
  num_live_vars++;
  printf("Registered live var \"%s\" (delta %i)\n", name, delta);
}

// Textures & Atlas

Texture* createTexture(unsigned width, unsigned height, int fill){
  GLuint renderedTexture;
  glGenTextures(1, &renderedTexture);
  //glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, renderedTexture);
  unsigned char *data = NULL;
  if (fill >= 0) {
    data = malloc(width * height * 3);
    memset(data, fill, width * height * 3);
  }
  
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
  
  if (data) { free (data); data = NULL; }

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  if (textures == NULL) { textures = hnew(MAX_TEXTURES); }

  Texture* tex = calloc(1, sizeof(Texture));
  tex->data = NULL;
  tex->w = width;
  tex->h = height;
  tex->atlas_x = 0;
  tex->atlas_y = 0;
  sprintf(tex->name, "tex%i", textures->length);
  tex->atlas[0] = 0;
  tex->id = renderedTexture;

  hsetp(textures, tex->name, (void*)tex);
  return tex;
}

Texture* loadTexture(const char *path){
  unsigned err;
  unsigned char* image;
  unsigned width, height;
  char name[MAX_NAME_LENGTH];
  filename(path, name);

  Texture* tex = calloc(1, sizeof(Texture));

  printf("Loading %s", path);

  err = lodepng_decode32_file(&image, &width, &height, path);
  if(err) {
    ERROR("%u: %s\n", err, lodepng_error_text(err));
    return 0;
  }

  printf(" (%ix%i)\n", width, height);

  GLuint texid;
  glGenTextures(1, &texid);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texid);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);
  
  // fill Texture struct
  tex->data = image;
  tex->w = width;
  tex->h = height;
  tex->atlas_x = 0;
  tex->atlas_y = 0;
  tex->atlas_w = 1;
  tex->atlas_h = 1;
  strcpy(tex->name, name);
  tex->atlas[0] = 0;
  tex->id = texid;
  if (textures == NULL) { textures = hnew(MAX_TEXTURES); }
  hsetp(textures, name, (void*)tex);

  //free(image);
  return tex;
}

void useTexture(Texture *tex){
  glBindTexture(GL_TEXTURE_2D, tex->id);
}

void loadAtlas (const char *path){
  unsigned w, h, x, y, version;
  char name[MAX_NAME_LENGTH];

  FILE *f = fopen(path, "rt");
  if (!f){ ERROR("Could not open %s\n", path); }

  char atlaspng[MAX_NAME_LENGTH];
  strcpy(atlaspng, path);
  *(strrchr(atlaspng, '.')) = 0;
  strcat(atlaspng, ".png");

  Texture* atlastex = loadTexture(atlaspng), *tex;

  fscanf(f, "atlas %u %u %u\n", &version, &w, &h);
  
  while(fscanf(f, "%s %u %u %u %u\n", name, &x, &y, &w, &h) > 0){
    tex = calloc(1, sizeof(Texture));
    tex->id = atlastex->id;
    strcpy(tex->name, name);
    strcpy(tex->atlas, atlastex->name);
    tex->w = w;
    tex->h = h;
    tex->atlas_x = (float)x / atlastex->w;
    tex->atlas_y = (float)y / atlastex->h;
    tex->atlas_w = (float)w / atlastex->w;
    tex->atlas_h = (float)h / atlastex->h;
    hsetp(textures, name, (void*)tex);
  }
  fclose(f);
}

Texture *getTexture(const char *name){
  return (Texture *)hgetp(textures, name, NULL);
}

void _deleteTextureData(void *p){
  Texture *tex = (Texture*)p;
  free(tex->data);
  printf("Deleted texture %s\n", tex->name);
  free(tex);
}

void deleteTextures(){
  hashTraverse(textures, _deleteTextureData);
  hdelete(&textures);
}


// Drawing & Render

// if tex == NULL, the bottom-right pixel of the atlas texture will be used.

void drawQuad(Texture *tex, float x, float y, float w, float h, unsigned char r, unsigned char g, unsigned char b, unsigned char a, float d1, float d2){
	
	static Texture *_white_tex = NULL; 
	SpriteVertex *vertex = vertex_data + (num_sprites * 6);
	for (int i = 0; i < 6; i++)
	{
		SpriteVertex *v = vertex + i;
		v->x = (( x - WIDTH2) / WIDTH2) + VERTEX_OFFSET_X[i] * (w * PIXEL_SIZE / WIDTH2);
		v->y = ((HEIGHT2 - y) / HEIGHT2) - VERTEX_OFFSET_Y[i] * (h * PIXEL_SIZE / HEIGHT2);
		if (tex == NULL){
			v->u = 1.0f;
			v->v = 1.0f;
		}
		else {
			v->u = tex->atlas_x + tex->atlas_w * UV_OFFSET_X[i];
			v->v = tex->atlas_y + tex->atlas_h * UV_OFFSET_Y[i];
		}
		v->r = r;
		v->g = g;
		v->b = b;
		v->a = a;
		v->d1 = d1;
		v->d2 = d2;
	}
	
	if (tex == NULL) {
		if (num_texture_batches == 0) {
			if (_white_tex == NULL) { _white_tex = createTexture(1, 1, 255); }
			tex = _white_tex;
		}
		else {
			tex = texture_batches[num_texture_batches - 1].texture;
		}
	}

	if (num_texture_batches == 0 || tex->id != texture_batches[num_texture_batches - 1].texture->id){
		texture_batches[num_texture_batches].start = num_sprites;
		texture_batches[num_texture_batches].texture = tex;
		num_texture_batches++;
	}

	num_sprites ++;

	if (num_sprites == MAX_SPRITES){ render(); }
}


void render(){
	if (num_sprites == 0) return;
	unsigned end, count;
	for (int i = 0; i < num_texture_batches; i++)
	{
		end = (i == num_texture_batches - 1) ? num_sprites : texture_batches[i + 1].start;
		count = end - texture_batches[i].start;
	  glBindTexture(GL_TEXTURE_2D, texture_batches[i].texture ? texture_batches[i].texture->id : 0);
		glBufferSubData(GL_ARRAY_BUFFER, 0, count * 6 * sizeof(SpriteVertex), (void*)(vertex_data + 6 * texture_batches[i].start));
		glDrawArrays(GL_TRIANGLES, 0, 6 * count);
	}
	num_sprites = 0;
	num_texture_batches = 0;
}

// Frame buffers

GLuint createFramebuffer(){
	GLuint fb;
	glGenFramebuffers(1, &fb);

	GLuint rbId = 0;
	glGenRenderbuffers(1, &rbId);
	glBindRenderbuffer(GL_RENDERBUFFER, rbId);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fb);
	glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, rbId);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	//todo: do this on window resize
	glBindRenderbuffer(GL_RENDERBUFFER, rbId);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, WIDTH / PIXEL_SIZE, HEIGHT / PIXEL_SIZE);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	return fb;
}

void beginFramebuffer(GLuint fb){
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fb);
	glViewport(0, 0, WIDTH / PIXEL_SIZE, HEIGHT / PIXEL_SIZE);
}

void endFramebuffer(GLuint fb){
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, fb);
	glBlitFramebuffer(0, 0, WIDTH / PIXEL_SIZE, HEIGHT / PIXEL_SIZE,
		                0, 0, WIDTH, HEIGHT,
		                GL_COLOR_BUFFER_BIT, GL_NEAREST);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}
