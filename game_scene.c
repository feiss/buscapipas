#include "game_scene.h"
#include <math.h>

#define COLS 10
#define ROWS 10
#define NUM_BOMBS 12

#define TILESIZE 20
#define STAGEX 20
#define STAGEY 60
#define HEADERY 20
#define STAGE(x,y) (stage[(y) * COLS + (x)])
#define STAGEDATA(x,y) (stagedata[(y) * COLS + (x)])

typedef enum {
	TDOWN, TUP, T1, T2, T3, T4, T5, T6, T7, T8, TFLAG, TBOMB, TKO, TUPBOMB, TCOUNT
} FlagTypes;

static double t = 0;
static int gametime;
static bool gameover, won;
static int flags = 0;
static int up_count;

extern Texture *tex_tileup, *tex_tileupbomb, *tex_tiledown, *tex_tile1, *tex_tile2, *tex_tile3, *tex_tile4, *tex_tile5, *tex_tile6, *tex_tile7, *tex_tile8, *tex_tileflag, *tex_tilebomb, *tex_tileko;
extern Texture *tex_smiley, *tex_numbers[11];

static Texture *TILES[13];

static char stagedata[ROWS*COLS];
static char stage[ROWS*COLS];

void draw_tile(Texture *tex, int x, int y, int r, int g, int b){
	drawQuad(tex, x, y, tex->w, tex->h, r, g, b, 255, 0.f, 0.f);
}

void clearTiles(int x, int y){
	if (x < 0 || x >= COLS || y < 0 || y >= ROWS) return;
	if (STAGE(x, y) == TUP && STAGEDATA(x, y) != TBOMB) {
		STAGE(x, y) = STAGEDATA(x, y);
		up_count--;
		if (STAGE(x, y) == 0) {
			clearTiles(x - 1, y);
			clearTiles(x + 1, y);
			clearTiles(x, y - 1);
			clearTiles(x, y + 1);

			clearTiles(x - 1, y - 1);
			clearTiles(x + 1, y - 1);
			clearTiles(x - 1, y + 1);
			clearTiles(x + 1, y + 1);
		}
	}
}

void print(int x, int y, char *s){
	for (int i = 0; i < strlen(s); i++) {
		if (s[i] == ':') { draw_tile(tex_numbers[10], x, y, 255, 255, 255); x += tex_numbers[10]->w - 3; }
		else { draw_tile(tex_numbers[s[i] - '0'], x, y, 255, 255, 255); x += tex_numbers[s[i] - '0']->w - 2; }
	}
}

bool mouseInTexture(Texture *tex, int x, int y){
	int mx = getMouseX();
	int my = getMouseY();
	return mx >= x && my >= y && mx < x + tex->w && my < y + tex->h;
}

void gamescene_processEvents(GLFWwindow *window){
	if (keyJustPressed(GLFW_KEY_Q)){
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	if (keyJustPressed(GLFW_KEY_F5)){
		t = 0;
		return;
	}
	if (leftJustPressed() && mouseInTexture(tex_smiley, WIDTH2 - TILESIZE/2, HEADERY)){
		t = 0;
		return;
	}

	if (!won && !gameover) {
		if (keyJustPressed(GLFW_KEY_F) || rightJustPressed()){
			int x = floor((getMouseX() - STAGEX) / TILESIZE);
			int y = floor((getMouseY() - STAGEY) / TILESIZE);
			if (STAGE(x, y) == TFLAG) { STAGE(x, y) = TUP; flags ++; }
			else if (STAGE(x, y) == TUP) { STAGE(x, y) = TFLAG; flags --; }
		}
		else if (leftJustPressed()){
			int x = floor((getMouseX() - STAGEX) / TILESIZE);
			int y = floor((getMouseY() - STAGEY) / TILESIZE);
			if (STAGE(x, y) != TUP) return;
			if (STAGEDATA(x, y) == TBOMB) {
				STAGEDATA(x,y) = TKO;
				gameover = true;
				return;
			}
			if (STAGEDATA(x, y) == TDOWN) clearTiles(x, y);
			else {
				STAGE(x, y) = STAGEDATA(x, y);
				up_count --;
			}
		}
	}
}

int countBombs(int x, int y){
	int bombs = 0;
	if (STAGEDATA(x,y) == TBOMB) bombs++;
	if (y > 0){
		if (x > 0 && STAGEDATA(x - 1, y - 1) == TBOMB) bombs++;
		if (STAGEDATA(x, y - 1) == TBOMB) bombs++;
		if (x < COLS - 1 && STAGEDATA(x + 1, y - 1) == TBOMB) bombs++;
	}
	if (x > 0 && STAGEDATA(x - 1, y) == TBOMB) bombs++;
	if (x < COLS - 1 && STAGEDATA(x + 1, y) == TBOMB) bombs++;
	if (y < ROWS - 1){
		if (x > 0 && STAGEDATA(x - 1, y + 1) == TBOMB) bombs++;
		if (STAGEDATA(x, y + 1) == TBOMB) bombs++;
		if (x < COLS - 1 && STAGEDATA(x + 1, y + 1) == TBOMB) bombs++;
	}
	return bombs;
}

void start_game(double time){
	TILES[TUP] = tex_tileup;
	TILES[TDOWN] = tex_tiledown;
	TILES[T1] = tex_tile1;
	TILES[T2] = tex_tile2;
	TILES[T3] = tex_tile3;
	TILES[T4] = tex_tile4;
	TILES[T5] = tex_tile5;
	TILES[T6] = tex_tile6;
	TILES[T7] = tex_tile7;
	TILES[T8] = tex_tile8;
	TILES[TFLAG] = tex_tileflag;
	TILES[TBOMB] = tex_tilebomb;
	TILES[TUPBOMB] = tex_tileupbomb;
	TILES[TKO] = tex_tileko;

	memset(stage, TUP, ROWS * COLS);
	memset(stagedata, TDOWN, ROWS * COLS);

	int bombs = 0, x, y;

  srand((int)(getMouseX() * 434352));

	while(bombs < NUM_BOMBS){
		x = rand() % COLS;
		y = rand() % ROWS;
		if (STAGEDATA(x, y) == TBOMB) continue;
		STAGEDATA(x, y) = TBOMB;
		bombs ++;
	}

	for (x = 0; x < COLS; x++){
		for (y = 0; y < ROWS; y++){
			if (STAGEDATA(x,y) != 0) continue;
			bombs = countBombs(x,y);
			STAGEDATA(x,y) = bombs == 0 ? 0 : T1 - 1 + bombs;
		}
	}

	flags = NUM_BOMBS;
	gameover = false;
	t = time;
	gametime = 0;
	won = false;
	up_count = COLS * ROWS;
}

bool isHovered(int x, int y){
	return floor((getMouseX() - STAGEX) / TILESIZE) == x && floor((getMouseY() - STAGEY) / TILESIZE) == y;
}

void gamescene_render(double time){
	//init
	if (t == 0) { 
		start_game(time); 
		return;
	}

	if (!won && !gameover){

		if (up_count == NUM_BOMBS){
			won = true;
			for (int i = 0; i < COLS * ROWS; i++) {
				if (stagedata[i] == TBOMB) stage[i] = TUPBOMB;
			}
			printf("YOU WIN!!!\n");		
		}

		gametime = time - t;
	}

	int x, y, hover;

	glClear(GL_COLOR_BUFFER_BIT);
	// bg
	drawQuad(NULL, 0, 0, WIDTH, HEIGHT, 100, 100, 100, 255, 0.f, 0.f);

	// stage
	for (x = 0; x < COLS; x++){
		for (y = 0; y < ROWS; y++){
			hover = isHovered(x, y) ? 255 : 230;
			draw_tile(
				gameover ? TILES[(int)STAGEDATA(x,y)] : TILES[(int)STAGE(x,y)],
				STAGEX + x * TILESIZE * PIXEL_SIZE,
				STAGEY + y * TILESIZE * PIXEL_SIZE,
				hover, hover, hover
			);
		}
	}

	draw_tile(tex_smiley, WIDTH2 - TILESIZE/2, HEADERY, 255, 255, 255);
	char str[10];
	sprintf(str, "%02i:%02i", (int)floor((gametime) / 60), (int)(gametime) % 60);
	print(30, HEADERY, str);
	sprintf(str, "%i", flags < 0 ? 0 : flags);
	print(163, HEADERY, str);
}
