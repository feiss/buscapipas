#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>
#include <locale.h>
#include <stdarg.h>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "vendor/lodepng.h"

#define WIDTH  240
#define HEIGHT 280
#define WIDTH2  (WIDTH / 2)
#define HEIGHT2 (HEIGHT / 2)
#define PIXEL_SIZE 1

#define EXIT_ON_ESCAPE

#define MAX_NAME_LENGTH 30 // for identifiers
#define MAX_TEXTURES 100 // for textures hash

#define MAX_SPRITES 2000 // for vbo

#ifndef bool
#define bool int
#define true 1
#define false 0
#endif

typedef struct{
  GLuint id;
  char name[MAX_NAME_LENGTH];
  char atlas[MAX_NAME_LENGTH];
  unsigned w, h;
  float atlas_x, atlas_y;
  float atlas_w, atlas_h;
  unsigned char* data;
} Texture;

void   ERROR        (const char *msg, ...);

void   filename     (const char *path, char *fname);
int    filesize     (const char *path);
char*  readfile     (const char *path);

GLFWwindow*   openWindow   (int w, int h, const char *title, bool fullscreen, bool debug);
void          closeWindow  ();

void setLiveVar(char *name, int *var, int delta);

#define GLFW_KEY_SHIFT   390
#define GLFW_KEY_CONTROL 391
#define GLFW_KEY_ALT     392

#define KEY_NOT_PRESSED   0
#define KEY_JUST_PRESSED  1
#define KEY_HELD          2
#define KEY_JUST_RELEASED 3

void   updateEvents      (GLFWwindow *window);
int    getKeyState       (int key);
bool   keyNotPressed     (int key);
bool   keyJustPressed    (int key);
bool   keyPressed        (int key);
bool   keyJustReleased   (int key);
double getMouseX         ();
double getMouseY         ();
bool   leftPressed   ();
bool   rightPressed  ();
bool   middlePressed ();
bool   leftJustPressed   ();
bool   rightJustPressed  ();
bool   middleJustPressed ();
bool   leftJustReleased  ();
bool   rightJustReleased ();
bool   middleJustReleased();

Texture*      loadTexture   (const char *path);
Texture*      createTexture (unsigned width, unsigned height, int fill);
void          useTexture    (Texture *tex);
void          loadAtlas     (const char *path);
Texture*      getTexture    (const char *name);
void          deleteTextures();

void drawQuad  (Texture *tex, 
								float x, 
								float y, 
								float w, 
								float h, 
								unsigned char r, 
								unsigned char g, 
								unsigned char b, 
								unsigned char a, 
								float d1, 
								float d2);
void render();

GLuint createFramebuffer();
void beginFramebuffer(GLuint fb);
void endFramebuffer(GLuint fb);

#endif
