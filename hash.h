#pragma once

#define HASH_CHAR 0
#define HASH_INT 1
#define HASH_UINT 2
#define HASH_FLOAT 3
#define HASH_STRING 4
#define HASH_POINTER 5

union HashType{
  char c;
  int i;
  unsigned int ui;
  float f;
  char *s;
  void *p;
};

struct HashNode{
  char *key;
  struct HashNode *next;
  union HashType value;
  char value_type;
  int value_size;
};

struct Hash{
  struct HashNode **table;
  int length;
  int size;
};

struct Hash* hnew(int size);
void hdelete(struct Hash **hash);

void hsetc(struct Hash *hash, const char *key, char value);
void hseti(struct Hash *hash, const char *key, int value);
void hsetui(struct Hash *hash, const char *key, unsigned int value);
void hsetf(struct Hash *hash, const char *key, float value);
void hsets(struct Hash *hash, const char *key, char *value);
void hsetp(struct Hash *hash, const char *key, void *value);

struct HashNode* hget(struct Hash *hash, const char *key);
char  hgetc(struct Hash *hash, const char *key, char default_value);
int   hgeti(struct Hash *hash, const char *key, int default_value);
unsigned int hgetui(struct Hash *hash, const char *key, unsigned int default_value);
float hgetf(struct Hash *hash, const char *key, float default_value);
char* hgets(struct Hash *hash, const char *key, char *default_value);
void* hgetp(struct Hash *hash, const char *key, void *default_value);

void  hunset(struct Hash *hash, const char *key);

void hashTraverse(struct Hash *hash, void (*traverseFunc)(void *p));
void hashDebug(struct Hash *hash, int full_debug, void (*debugFunc)(void *p));

