#ifndef GAME_SCENE
#define GAME_SCENE
#include "graphics.h"

void gamescene_processEvents(GLFWwindow *window);
void gamescene_render(double time);

#endif